FROM centos:7

RUN yum install python3 python3-pip -y
RUN pip3 install flask flask_restful flask-jsonpify
RUN mkdir /python_api
COPY python-api.py /python_api/python-api.py
WORKDIR /python_api
CMD ["python3", "/python_api/python-api.py"]

